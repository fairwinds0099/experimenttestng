#!groovy

REPORT_PATH="**/test-output/extent.html"
BRANCH= '${BRANCH}'
SUITE = '${SUITEXML}'

node('NODE_NAME') {
    stage ('checkout code') {
        git changelog: false,
                url: 'https://gitlab.com/fairwinds0099/experimenttestng.git',
                branch: '${BRANCH}'

    }

    currentBuild.result='SUCCESS'

    try {
        stage('mvn test') {
            MVN_CMD="${env.M3}/bin/mvn clean compile test -DsuiteXmlFile=${SUITE}"
            echo ${MVN_CMD}
            sh MVN_CMD
        }
    } catch (caughtError) {
        echo caughtError
        currentBuild.result='FAILURE'
    }

    stage('Publish results') {
        dir("project folder name") {
            publishHTML(
                    [
                            allowMissing: true,
                            alwaysLinkToLastBuild: true,
                            keepAll: true,
                            reportDir: '',
                            reportFiles: "${REPORT_PATH}",
                            reportName: 'Test results',
                            reportTitles: ''
                    ]
            )
        }
     }

    stage('email') {
       def summary = """""<!DOCTYPE html>
       body{}
       table{}
               """

        emailext(
                to: 'fairwinds0099@gmail.com',
                subject: 'test results',
                attachLog: true,
                body: summary,
                mimeType = 'text/html'
        )
    }

    stage('publish junit') {
        junit '**/target/surefire-reports/junitreports/*.xml'
    }
}